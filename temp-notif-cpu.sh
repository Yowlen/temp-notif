#!/bin/bash

echo "Temperature Notifier"
echo "Press CTRL-C to stop monitoring."
while true; do
    # Get the temperature.
    #
    # First grep command tracks the sensor module.
    # The number following the -A parameter is the number
    # of lines to send on to the rest, and the part in
    # quotes is a string to uniquely idenitfy the module.
    #
    # The second just uses the string of the actual sensor
    # we want to read.
    #
    # Adjust the values as needed, using the output of the
    # sensors command to guide you.
    #
    # Base script for most AMD CPUs
    t=$(sensors | grep -A 3 "k10temp" | grep "Tdie" | cut -f 2 -d "+" | cut -c 1-3)
    # Base script for most Intel CPUs
    # t=$(sensors | grep -A 3 "coretemp" | grep "Package id 0" | cut -f 2 -d "+" | cut -c 1-3)

    # Throw an error and exit if we didn't get anything.
    if [ $t = "" ]; then exit 1; fi

    # Trim it so that we only have an integer.
    if [ $(echo $t | cut -c 3) = "." ]; then
        t=$(echo $t | cut -c 1-2)
    fi

    # Enable this command if you wanna create a log file.
    #echo "$t C" >> ~/temp-notif-cpu.log

    # Temperature check time
    #
    # The three checks combined basically work their way down
    # to determine if the first digit is too high, then to
    # the second for fine-tuned control.
    if [[ $(echo $t | cut -c 1) =~ ^(6|7|8|9|1)$ ]]; then
        notify-send --expire-time=4500 --app-name="Temperature Notifier" "Warning!" "CPU Temperature is $t Celcius!"
    #else
    #    if [[ $(echo $t | cut -c 1) = "8" ]]; then
    #        if [[ $(echo $t | cut -c 2) =~ ^(5|6|7|8|9)$ ]]; then
    #            notify-send --expire-time=4500 --app-name="Temperature Notifier" "Warning!" "CPU Temperature is $t Celcius!"
    #        fi
    #    fi
    fi

    # Wait just a bit so the notification can time out, if issued,
    # as well as ensure we don't actually overheat the CPU just
    # from checking the temperature.
    sleep 5
done

