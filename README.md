# temp-notif

A quick and easy temperature notification script for bash shells.

# Requirements
- `sensors` must be installed and configured.
- Note that this is set up to work with AMD's `k10temp` and `amdgpu` for my Ryzen 2600 and Radeon R9 290x by default and may need adjustments, even for other AMD hardware. Instructions included in the script's comments.

# Usage
After configuration, just run it in a terminal and it'll automatically flash notifications.

